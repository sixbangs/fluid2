#version 460 core

#include <flutter/runtime_effect.glsl>

precision mediump float;

uniform vec2 uSize;
uniform vec3 uColor;

uniform sampler2D uGradientTexture;

out vec4 fragColor;

void main() {
    vec2 uv = FlutterFragCoord().xy / uSize;
    vec4 gradientColor = texture(uGradientTexture, uv);

    float smoothedAlpha = smoothstep(0.92, 1.0, gradientColor.a);
    fragColor = vec4(uColor * smoothedAlpha, smoothedAlpha);
}