#version 460 core

#include <flutter/runtime_effect.glsl>

precision mediump float;

uniform float uGradientRadius;

out vec4 fragColor;

float falloff(float r) {
    if (r < 1.0) {
      float r2 = r * r;
      return (1.0 - r2) * (1.0 - r2) * (1.0 - r2) * (1.0 - r2);
    } else {
      return 0.0;
    }
}

void main() {
    vec2 center = vec2(uGradientRadius, uGradientRadius);
    vec2 diff = FlutterFragCoord().xy - center;
    float dist = length(diff);

    float alpha = falloff(dist / uGradientRadius);

    vec3 white = vec3(1.0, 1.0, 1.0);
    fragColor = vec4(white * alpha, alpha);
}