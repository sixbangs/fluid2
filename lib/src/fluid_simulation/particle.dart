import 'package:vector_math/vector_math.dart';

class Particle {
  Vector2 position;
  Vector2 oldPosition;
  Vector2 velocity;
  double p;
  double pNear;
  double g;

  Particle(double x, double y)
      : position = Vector2(x, y),
        oldPosition = Vector2(x, y),
        velocity = Vector2(0, 0),
        p = 0,
        pNear = 0,
        g = 0;
}
