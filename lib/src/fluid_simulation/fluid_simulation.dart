import 'dart:math';

import 'package:vector_math/vector_math.dart';
import 'package:spatial_hash_new/spatial_hash_new.dart';

import 'particle.dart';

class FluidSimulation {
  int particleCount;
  double stiffness;
  double stiffnessNear;
  double restDensity;
  double interactionRadius;
  double gravityMagnitude;

  final List<Particle> _particles = [];
  List<Particle> get particles => _particles;

  final Random _random;

  SpatialHash<Particle> _hashMap;
  int _previousWidth = 0;
  int _previousHeight = 0;

  FluidSimulation(
    this.particleCount,
    this.stiffness,
    this.stiffnessNear,
    this.restDensity,
    this.interactionRadius,
    this.gravityMagnitude,
  )   : _random = Random(),
        _hashMap = SpatialHash<Particle>(1, 1, 1, 1);

  void update(
    final double dt,
    final int width,
    final int height,
    Vector2? pointerPosition,
    Vector3? absoluteOrientation,
  ) {
    if (width != _previousWidth || height != _previousHeight) {
      _previousWidth = width;
      _previousHeight = height;
      _hashMap = _initSpatialHash(width, height);
      for (final Particle particle in _particles) {
        _hashMap.add(particle, _particleRectangle(particle.position));
      }
    }

    _addOrRemoveParticles(width, height);

    for (final Particle particle in _particles) {
      particle.oldPosition = particle.position;
      _applyGlobalForces(particle, dt, pointerPosition, absoluteOrientation);
      particle.position += particle.velocity * dt;

      _hashMap.update(particle, _particleRectangle(particle.position));
    }

    for (final Particle particle in _particles) {
      List<Particle> neighbors = _findNeighbors(_hashMap, particle);

      _updatePressure(particle, neighbors);
      _relax(particle, neighbors, dt);
    }

    for (final Particle particle in _particles) {
      _contain(particle, width, height);
      _calculateVelocity(particle, dt);
    }
  }

  SpatialHash<Particle> _initSpatialHash(int width, int height) {
    final cellSize = interactionRadius * 2;
    final int cellsX = (width / cellSize).ceil();
    final int cellsY = (height / cellSize).ceil();
    return SpatialHash<Particle>(cellsX, cellsY, cellSize, cellSize);
  }

  void _addOrRemoveParticles(int width, int height) {
    if (_particles.length < particleCount) {
      final centerX = width / 2;
      final centerY = height / 2;
      final radius = width / 4;

      int add = min(2, particleCount - _particles.length);
      for (int i = 0; i < add; i++) {
        Particle particle = _addParticle(centerX, centerY, radius);
        _hashMap.add(particle, _particleRectangle(particle.position));
      }
    } else if (_particles.length > particleCount) {
      int remove = min(2, _particles.length - particleCount);
      for (int i = 0; i < remove; i++) {
        Particle particle = _particles.removeLast();
        _hashMap.remove(particle);
      }
    }
  }

  Particle _addParticle(double centerX, double centerY, double radius) {
    final angle = _random.nextDouble() * 2 * pi;
    final distance = sqrt(_random.nextDouble()) * radius;

    final x = centerX + distance * cos(angle);
    final y = centerY + distance * sin(angle);

    Particle particle = Particle(x, y);
    _particles.add(particle);
    return particle;
  }

  Rectangle _particleRectangle(Vector2 position) {
    return Rectangle(
        position.x, position.y, interactionRadius * 2, interactionRadius * 2);
  }

  void _applyGlobalForces(
    Particle particle,
    double dt,
    Vector2? pointerPosition,
    Vector3? absoluteOrientation,
  ) {
    Vector2 gravityDirection = _calculateGravityVector(absoluteOrientation);
    Vector2 force = gravityDirection * gravityMagnitude;
    force += _calculatePointerForce(particle, pointerPosition);

    particle.velocity += force * dt;
  }

  Vector2 _calculateGravityVector(Vector3? absoluteOrientation) {
    if (absoluteOrientation == null) {
      return Vector2(0, 1);
    }

    final double gravityX = sin(absoluteOrientation.z);
    final double gravityY = sin(absoluteOrientation.x);

    // Create and normalize the gravity vector
    Vector2 gravity2D = Vector2(gravityX, gravityY);
    gravity2D.normalize();
    return gravity2D;
  }

  Vector2 _calculatePointerForce(Particle particle, Vector2? pointerPosition) {
    if (pointerPosition == null) {
      return Vector2.zero();
    }

    Vector2 fromPointer = particle.position - pointerPosition;
    double scalar = min(40000, 4000000 / fromPointer.length2);
    return fromPointer.normalized() * scalar;
  }

  List<Particle> _findNeighbors(
    SpatialHash<Particle> hashMap,
    Particle particle,
  ) {
    List<Particle> neighbors = [];
    for (final neighbor in hashMap.near(particle)) {
      double? g = _gradient(particle, neighbor);
      if (g != null) {
        neighbor.g = g;
        neighbors.add(neighbor);
      }
    }
    return neighbors;
  }

  double? _gradient(Particle a, Particle b) {
    final dx = a.position.x - b.position.x;
    final dy = a.position.y - b.position.y;
    final distanceSquared = dx * dx + dy * dy;

    if (distanceSquared > interactionRadius * interactionRadius) {
      return null;
    }

    final distance = sqrt(distanceSquared);
    return 1 - distance / interactionRadius;
  }

  void _updatePressure(Particle particle, List<Particle> neighbors) {
    double density = 0;
    double nearDensity = 0;

    for (final Particle neighbor in neighbors) {
      double g = neighbor.g;
      density += g * g;
      nearDensity += g * g * g;
    }

    particle.p = stiffness * (density - restDensity);
    particle.pNear = stiffnessNear * nearDensity;
  }

  void _relax(Particle particle, List<Particle> neighbors, double dt) {
    // Optimized to reduce Vector2 clones
    final px = particle.position.x;
    final py = particle.position.y;

    for (final neighbor in neighbors) {
      final g = neighbor.g;

      final dx = neighbor.position.x - px;
      final dy = neighbor.position.y - py;
      final distance = sqrt(dx * dx + dy * dy) +
          1e-6; // Add a small epsilon to avoid division by zero
      final directionX = dx / distance;
      final directionY = dy / distance;

      final magnitude = particle.p * g + particle.pNear * g * g;
      final forceX = directionX * magnitude;
      final forceY = directionY * magnitude;

      final dtSquared = dt * dt;
      final displacementX = forceX * dtSquared;
      final displacementY = forceY * dtSquared;

      particle.position.x -= displacementX * 0.5;
      particle.position.y -= displacementY * 0.5;
      neighbor.position.x += displacementX * 0.5;
      neighbor.position.y += displacementY * 0.5;
    }
  }

  void _contain(Particle particle, int width, int height) {
    final w = width.toDouble();
    final h = height.toDouble();
    if (particle.position.x < 0) {
      particle.oldPosition.x = particle.position.x;
      particle.position.x = 0;
    } else if (particle.position.x > w) {
      particle.oldPosition.x = particle.position.x;
      particle.position.x = w;
    }
    if (particle.position.y < 0) {
      particle.oldPosition.y = particle.position.y;
      particle.position.y = 0;
    } else if (particle.position.y > h) {
      particle.oldPosition.y = particle.position.y;
      particle.position.y = h;
    }
  }

  void _calculateVelocity(Particle particle, double dt) {
    particle.velocity = (particle.position - particle.oldPosition) * (1 / dt);

    // Speed limit
    const double speedLimit = 1000.0;
    if (particle.velocity.length2 > speedLimit * speedLimit) {
      particle.velocity = particle.velocity.normalized() * speedLimit;
    }
  }
}
