import 'orientation.dart';

class OrientationListener {
  void startListening(OrientationCallback onOrientationChanged) {}

  void stopListening() {}
}
