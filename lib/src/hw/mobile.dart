import 'dart:io';

import 'package:motion_sensors/motion_sensors.dart';
import 'package:vector_math/vector_math.dart';
import 'orientation.dart';

class OrientationListener {
  void startListening(OrientationCallback onOrientationChanged) {
    if (_isDesktop()) {
      return;
    }

    motionSensors.absoluteOrientation.listen((AbsoluteOrientationEvent event) {
      final orientation = Vector3(event.pitch, event.yaw, event.roll);
      onOrientationChanged(orientation);
    });
  }

  void stopListening() {
    if (!_isDesktop()) {
      motionSensors.absoluteOrientation.drain();
    }
  }

  bool _isDesktop() =>
      Platform.isMacOS || Platform.isLinux || Platform.isWindows;
}
