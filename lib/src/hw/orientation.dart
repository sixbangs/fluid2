import 'package:vector_math/vector_math.dart';

typedef OrientationCallback = void Function(Vector3 orientation);
