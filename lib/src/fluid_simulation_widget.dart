import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:vector_math/vector_math.dart';

import 'fluid_simulation/fluid_simulation.dart';

import 'hw/none.dart' if (dart.library.io) 'hw/mobile.dart';

import 'painters.dart';

/// The default number of particles in the simulation.
const int kDefaultParticleCount = 250;

/// The default stiffness constant for the standard density constraint.
const double kDefaultStiffness = 90;

/// The default stiffness constant for the near-density constraint.
const double kDefaultStiffnessNear = 120;

/// The default rest density of the fluid.
const double kDefaultRestDensity = 10;

/// The default radius around a particle in which it interacts with neighboring particles.
const double kDefaultInteractionRadius = 60;

/// The default magnitude of the gravity force applied to the particles.
const double kDefaultGravityMagnitude = 0;

/// The default render mode of the particles.
const RenderMode kDefaultRenderMode = RenderMode.blobs;

/// The default radius of the density gradient used for rendering blobs.
const double kDefaultGradientRadius = 48;

/// A widget that renders a fluid simulation.
///
/// The simulation is based on double-density relaxation and supports interactivity via tap or mouse hover and device orientation.
///
/// Ported from [LÖVE Fluid Simulation](https://gitlab.com/sixbangs/fluid) with significant help from [Claude](https://claude.ai).
///
/// See also:
///
/// * [Particle-based Viscoelastic Fluid Simulation](https://gitlab.com/dumbtech/fluid2/-/raw/main/doc/pvfs.pdf)
///   The paper describing double-density relaxation.
/// * [Simulating Blobs of Fluid](https://peeke.nl/simulating-blobs-of-fluid)
///   The article that inspired me to build my first fluid simulation.
class FluidSimulationWidget extends StatefulWidget {
  /// The color of the fluid.
  ///
  /// Defaults to the [ColorScheme.primary] of the current [Theme] if not specified.
  final Color? color;

  /// The number of particles in the simulation.
  ///
  /// Defaults to [kDefaultParticleCount].
  final int particleCount;

  /// The stiffness constant for the pressure constraint.
  ///
  /// Determines how strongly the fluid resists compression. Higher values make the fluid less compressible
  /// and more resistant to changes in density. Defaults to [kDefaultStiffness].
  final double stiffness;

  /// The stiffness constant for the near-pressure constraint.
  ///
  /// Determines the additional pressure force applied between particles that are closer together.
  /// Higher values create stronger repulsive forces at close distances, which can help to prevent
  /// particle clustering and maintain a more uniform density distribution. Defaults to [kDefaultStiffnessNear].
  final double stiffnessNear;

  /// The rest density of the fluid.
  ///
  /// Higher values result in a denser fluid. Defaults to [kDefaultRestDensity].
  final double restDensity;

  /// The radius around a particle in which it interacts with neighboring particles.
  ///
  /// Defaults to [kDefaultInteractionRadius].
  final double interactionRadius;

  /// The magnitude of the gravity force applied to the particles.
  ///
  /// Defaults to [kDefaultGravityMagnitude].
  final double gravityMagnitude;

  /// The render mode of the particles.
  ///
  /// Either as individual [RenderMode.points] or metaballs ([RenderMode.blobs]). Defaults to [kDefaultRenderMode].
  final RenderMode renderMode;

  /// The radius of the density gradient used for rendering blobs.
  ///
  /// Only applicable in [RenderMode.blobs] mode. Defaults to [kDefaultGradientRadius].
  final double gradientRadius;

  /// Whether the fluid particles are affected by tap or mouse hover.
  ///
  /// Defaults to true.
  final bool pointerEnabled;

  /// Whether gravity is determined by the device orientation.
  ///
  /// Defaults to true. Only supported on mobile.
  final bool orientationEnabled;

  /// An optional child widget to render UI elements on top of the fluid simulation.
  final Widget? child;

  const FluidSimulationWidget({
    super.key,
    this.color,
    this.particleCount = kDefaultParticleCount,
    this.stiffness = kDefaultStiffness,
    this.stiffnessNear = kDefaultStiffnessNear,
    this.restDensity = kDefaultRestDensity,
    this.interactionRadius = kDefaultInteractionRadius,
    this.gravityMagnitude = kDefaultGravityMagnitude,
    this.renderMode = kDefaultRenderMode,
    this.gradientRadius = kDefaultGradientRadius,
    this.pointerEnabled = true,
    this.orientationEnabled = true,
    this.child,
  });

  @override
  State<FluidSimulationWidget> createState() => _FluidSimulationWidgetState();
}

class _FluidSimulationWidgetState extends State<FluidSimulationWidget>
    with SingleTickerProviderStateMixin {
  late final FluidSimulation _simulation;
  late final OrientationListener _orientationListener;
  late final AnimationController _animationController;

  int? _lastUpdateTimestamp;

  Vector2? _pointerPosition;
  Vector3? _orientation;

  ui.FragmentShader? _gradientShader;
  ui.FragmentShader? _blobShader;

  @override
  void initState() {
    super.initState();

    _simulation = FluidSimulation(
      widget.particleCount,
      widget.stiffness,
      widget.stiffnessNear,
      widget.restDensity,
      widget.interactionRadius,
      widget.gravityMagnitude,
    );

    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(days: 1),
    )..repeat();

    _orientationListener = OrientationListener();
    _orientationListener.startListening((orientation) {
      _orientation = orientation;
    });

    _loadShaders();
  }

  @override
  void didUpdateWidget(FluidSimulationWidget oldWidget) {
    super.didUpdateWidget(oldWidget);

    _simulation
      ..particleCount = widget.particleCount
      ..stiffness = widget.stiffness
      ..stiffnessNear = widget.stiffnessNear
      ..restDensity = widget.restDensity
      ..interactionRadius = widget.interactionRadius
      ..gravityMagnitude = widget.gravityMagnitude;
  }

  void _loadShaders() async {
    ui.FragmentShader gradients = await _loadShader('gradient');
    ui.FragmentShader blobs = await _loadShader('blobs');
    setState(() {
      _gradientShader = gradients;
      _blobShader = blobs;
    });
  }

  Future<ui.FragmentShader> _loadShader(String name) async {
    ui.FragmentProgram program = await ui.FragmentProgram.fromAsset(
        "packages/fluid_simulation/shaders/$name.frag");
    return program.fragmentShader();
  }

  double _calculateDeltaTime() {
    if (_lastUpdateTimestamp == null) {
      return 1;
    }

    final now = DateTime.now().microsecondsSinceEpoch;
    return (now - _lastUpdateTimestamp!) / 1000000;
  }

  @override
  Widget build(BuildContext context) {
    Vector2? globalPositionToLocal(Offset offset) {
      RenderBox renderBox = context.findRenderObject() as RenderBox;
      Offset localPosition = renderBox.globalToLocal(offset);
      if (localPosition.dx < 0 ||
          localPosition.dx > renderBox.size.width ||
          localPosition.dy < 0 ||
          localPosition.dy > renderBox.size.height) {
        return null;
      }
      return Vector2(localPosition.dx, localPosition.dy);
    }

    onFingerLifted() {
      _pointerPosition = null;
    }

    onBeforePaint(Size size) {
      final double dt = _calculateDeltaTime();
      final int width = size.width.toInt();
      final int height = size.height.toInt();

      _simulation.update(
        dt,
        width,
        height,
        widget.pointerEnabled ? _pointerPosition : null,
        widget.orientationEnabled ? _orientation : null,
      );

      _lastUpdateTimestamp = DateTime.now().microsecondsSinceEpoch;
    }

    return GestureDetector(
        onTapDown: (details) {
          _pointerPosition = globalPositionToLocal(details.globalPosition);
        },
        onPanUpdate: (details) {
          _pointerPosition = globalPositionToLocal(details.globalPosition);
        },
        onTapUp: (_) {
          onFingerLifted();
        },
        onPanEnd: (_) {
          onFingerLifted();
        },
        child: MouseRegion(
          onHover: (event) {
            _pointerPosition = globalPositionToLocal(event.position);
          },
          child: _shadersLoaded()
              ? LayoutBuilder(
                  builder: (context, constraints) {
                    final size = constraints.biggest;
                    final color =
                        widget.color ?? Theme.of(context).colorScheme.primary;

                    return AnimatedBuilder(
                      animation: _animationController,
                      builder: (BuildContext context, Widget? child) {
                        return CustomPaint(
                          size: size,
                          painter: switch (widget.renderMode) {
                            RenderMode.points => PointPainter(
                                particles: _simulation.particles,
                                color: color,
                                onBeforePaint: onBeforePaint,
                              ),
                            RenderMode.blobs => BlobPainter(
                                particles: _simulation.particles,
                                color: color,
                                gradientRadius: widget.gradientRadius,
                                gradientShader: _gradientShader!,
                                blobShader: _blobShader!,
                                onBeforePaint: onBeforePaint,
                              ),
                          },
                          child: widget.child,
                        );
                      },
                    );
                  },
                )
              : widget.child,
        ));
  }

  bool _shadersLoaded() => _gradientShader != null && _blobShader != null;

  @override
  void dispose() {
    _animationController.dispose();
    _orientationListener.stopListening();
    super.dispose();
  }
}

/// The render mode of the fluid particles.
enum RenderMode {
  /// Render particles as individual points.
  points,

  /// Render particles as metaballs, creating a blobby fluid surface.
  blobs,
}
