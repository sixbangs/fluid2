import 'dart:ui' as ui;

import 'package:flutter/material.dart';

import 'fluid_simulation/particle.dart';

typedef OnBeforePaintCallback = void Function(Size);

sealed class ParticlePainter extends CustomPainter {
  final List<Particle> _particles;
  final Color _color;
  final OnBeforePaintCallback _onBeforePaint;

  ParticlePainter(
    List<Particle> particles,
    Color color,
    OnBeforePaintCallback onBeforePaint,
  )   : _particles = particles,
        _color = color,
        _onBeforePaint = onBeforePaint;

  @override
  void paint(Canvas canvas, Size size) {
    _onBeforePaint(size);
  }

  @override
  bool shouldRepaint(ParticlePainter oldDelegate) => true;
}

class PointPainter extends ParticlePainter {
  final ui.Image _particleImage;

  PointPainter({
    required List<Particle> particles,
    required Color color,
    required OnBeforePaintCallback onBeforePaint,
  })  : _particleImage = _createParticleImage(color),
        super(particles, color, onBeforePaint);

  static ui.Image _createParticleImage(Color color) {
    final recorder = ui.PictureRecorder();
    final canvas = Canvas(recorder);

    final paint = Paint()
      ..style = PaintingStyle.fill
      ..color = color
      ..isAntiAlias = true;

    canvas.drawCircle(const Offset(5, 5), 1.5, paint);

    final picture = recorder.endRecording();
    return picture.toImageSync(10, 10);
  }

  @override
  void paint(Canvas canvas, Size size) {
    super.paint(canvas, size);

    final paint = Paint();
    const spriteRect = Rect.fromLTWH(0, 0, 10, 10);

    final transforms = <RSTransform>[];
    final rects = <Rect>[];
    for (final particle in _particles) {
      final x = particle.position.x;
      final y = particle.position.y;

      transforms.add(RSTransform.fromComponents(
        rotation: 0,
        scale: 1,
        anchorX: 0,
        anchorY: 0,
        translateX: x - 5,
        translateY: y - 5,
      ));
      rects.add(spriteRect);
    }

    canvas.drawAtlas(
      _particleImage,
      transforms,
      rects,
      null,
      null,
      null,
      paint,
    );
  }
}

class BlobPainter extends ParticlePainter {
  final ui.FragmentShader _blobShader;
  final double _gradientRadius;

  late final ui.Image _gradientImage;

  BlobPainter({
    required List<Particle> particles,
    required Color color,
    required double gradientRadius,
    required ui.FragmentShader gradientShader,
    required ui.FragmentShader blobShader,
    required OnBeforePaintCallback onBeforePaint,
  })  : _blobShader = blobShader,
        _gradientRadius = gradientRadius,
        super(particles, color, onBeforePaint) {
    _gradientImage = _createGradient(gradientShader);
  }

  ui.Image _createGradient(ui.FragmentShader gradientShader) {
    final recorder = ui.PictureRecorder();
    final canvas = Canvas(recorder);

    final width = _gradientRadius * 2;
    final height = _gradientRadius * 2;

    gradientShader.setFloat(0, _gradientRadius);

    canvas.drawRect(
      Rect.fromLTWH(0, 0, width, height),
      Paint()..shader = gradientShader,
    );

    final picture = recorder.endRecording();
    return picture.toImageSync(width.toInt(), height.toInt());
  }

  @override
  void paint(Canvas canvas, Size size) {
    super.paint(canvas, size);

    final gradientTexture = _createGradientTexture(size.width, size.height);

    final shader = _blobShader;
    shader.setFloat(0, size.width);
    shader.setFloat(1, size.height);
    shader.setFloat(2, _color.red / 255.0);
    shader.setFloat(3, _color.green / 255.0);
    shader.setFloat(4, _color.blue / 255.0);

    shader.setImageSampler(0, gradientTexture);

    canvas.drawRect(
      Rect.fromLTWH(0, 0, size.width, size.height),
      Paint()
        ..shader = shader
        ..isAntiAlias = true,
    );
  }

  ui.Image _createGradientTexture(double width, double height) {
    final recorder = ui.PictureRecorder();
    final canvas = Canvas(recorder);

    final paint = Paint()..blendMode = BlendMode.plus;
    final spriteRect = Rect.fromLTWH(
      0,
      0,
      _gradientRadius * 2,
      _gradientRadius * 2,
    );

    final transforms = <RSTransform>[];
    final rects = <Rect>[];
    for (final particle in _particles) {
      final x = particle.position.x;
      final y = particle.position.y;

      transforms.add(RSTransform.fromComponents(
        rotation: 0,
        scale: 1,
        anchorX: 0,
        anchorY: 0,
        translateX: x - _gradientRadius,
        translateY: y - _gradientRadius,
      ));
      rects.add(spriteRect);
    }

    canvas.drawAtlas(
      _gradientImage,
      transforms,
      rects,
      null,
      null,
      null,
      paint,
    );

    final picture = recorder.endRecording();
    return picture.toImageSync(width.toInt(), height.toInt());
  }
}
