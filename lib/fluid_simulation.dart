/// A widget that renders an interactive fluid simulation.
library fluid_simulation;

export 'src/fluid_simulation_widget.dart';
