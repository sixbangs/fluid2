import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluid_simulation/fluid_simulation.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:url_launcher/url_launcher.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]).then((_) {
    runApp(const MyApp());
  });
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  late int particleCount;
  late double stiffness;
  late double stiffnessNear;
  late double restDensity;
  late double interactionRadius;
  late double gravityMagnitude;
  late RenderMode renderMode;
  late double gradientRadius;

  @override
  void initState() {
    super.initState();
    _resetParameters();
  }

  void _resetParameters() {
    particleCount = kDefaultParticleCount;
    stiffness = kDefaultStiffness;
    stiffnessNear = kDefaultStiffnessNear;
    restDensity = kDefaultRestDensity;
    interactionRadius = kDefaultInteractionRadius;
    gravityMagnitude = kDefaultGravityMagnitude;
    renderMode = kDefaultRenderMode;
    gradientRadius = kDefaultGradientRadius;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fluid Simulation',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(
          seedColor: Colors.blueGrey,
          brightness: Brightness.dark,
        ),
        useMaterial3: true,
      ),
      home: Builder(
        builder: (context) {
          return Scaffold(
            backgroundColor: Colors.black,
            drawerScrimColor: Colors.transparent,
            drawer: Drawer(
              width: 200,
              child: SafeArea(
                minimum: const EdgeInsets.only(top: 16),
                child: _buildControlPanel(context),
              ),
            ),
            body: LayoutBuilder(
              builder: (context, constraints) {
                return Stack(
                  children: [
                    Center(
                      child: SizedBox(
                        width: min(constraints.maxWidth, 500),
                        height: constraints.maxHeight,
                        child: FluidSimulationWidget(
                          particleCount: particleCount,
                          stiffness: stiffness,
                          stiffnessNear: stiffnessNear,
                          restDensity: restDensity,
                          interactionRadius: interactionRadius,
                          gravityMagnitude: gravityMagnitude,
                          renderMode: renderMode,
                          gradientRadius: gradientRadius,
                        ),
                      ),
                    ),
                    SafeArea(
                      minimum: const EdgeInsets.all(16),
                      child: Stack(
                        children: [
                          Align(
                            alignment: AlignmentDirectional.topStart,
                            child: IconButton.filledTonal(
                              onPressed: () =>
                                  Scaffold.of(context).openDrawer(),
                              icon: const Icon(Icons.menu),
                            ),
                          ),
                          Align(
                            alignment: AlignmentDirectional.topEnd,
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                SocialMediaButton(
                                  url: 'https://gitlab.com/dumbtech/fluid2',
                                  icon: 'gitlab',
                                ),
                                SocialMediaButton(
                                  url: 'https://twitter.com/SixBangs',
                                  icon: 'x',
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                );
              },
            ),
          );
        },
      ),
    );
  }

  Widget _buildControlPanel(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 16,
        right: 16,
        bottom: 16,
      ),
      child: SingleChildScrollView(
        child: DefaultTextStyle(
          style: TextStyle(
            fontSize: 12,
            color: Theme.of(context).colorScheme.onSurface,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              const Text(
                'Control Panel',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 24),
              const Text(
                'renderMode',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 8),
                child: Center(
                  child: ToggleButtons(
                    isSelected: [
                      renderMode == RenderMode.points,
                      renderMode == RenderMode.blobs,
                    ],
                    onPressed: (index) {
                      setState(() {
                        renderMode =
                            index == 0 ? RenderMode.points : RenderMode.blobs;
                      });
                    },
                    textStyle: const TextStyle(fontSize: 12),
                    constraints: const BoxConstraints(
                      minHeight: 30,
                    ),
                    borderRadius: BorderRadius.circular(8),
                    children: const [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12),
                        child: Text('points'),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 12),
                        child: Text('blobs'),
                      ),
                    ],
                  ),
                ),
              ),
              SliderControl(
                label: 'particleCount',
                value: particleCount.toDouble(),
                min: 0,
                max: 500,
                onChanged: (value) {
                  setState(() {
                    particleCount = value.toInt();
                  });
                },
              ),
              SliderControl(
                label: 'gravityMagnitude',
                value: gravityMagnitude,
                min: 0,
                max: 1000,
                onChanged: (value) {
                  setState(() {
                    gravityMagnitude = value;
                  });
                },
              ),
              SliderControl(
                label: 'stiffness',
                value: stiffness,
                min: 0,
                max: 200,
                onChanged: (value) {
                  setState(() {
                    stiffness = value;
                  });
                },
              ),
              SliderControl(
                label: 'stiffnessNear',
                value: stiffnessNear,
                min: 0,
                max: 200,
                onChanged: (value) {
                  setState(() {
                    stiffnessNear = value;
                  });
                },
              ),
              SliderControl(
                label: 'restDensity',
                value: restDensity,
                min: 0,
                max: 20,
                onChanged: (value) {
                  setState(() {
                    restDensity = value;
                  });
                },
              ),
              SliderControl(
                label: 'interactionRadius',
                value: interactionRadius,
                min: 10,
                max: 100,
                onChanged: (value) {
                  setState(() {
                    interactionRadius = value;
                  });
                },
              ),
              SliderControl(
                label: 'gradientRadius',
                value: gradientRadius.toDouble(),
                min: 5,
                max: 75,
                onChanged: (value) {
                  setState(() {
                    gradientRadius = value;
                  });
                },
              ),
              const SizedBox(height: 8),
              FilledButton.tonal(
                onPressed: () {
                  setState(() {
                    _resetParameters();
                  });
                },
                child: const Text('Reset'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SliderControl extends StatelessWidget {
  final String label;
  final double value;
  final double min;
  final double max;
  final ValueChanged<double> onChanged;

  const SliderControl({
    super.key,
    required this.label,
    required this.value,
    required this.min,
    required this.max,
    required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              label,
              style: const TextStyle(fontWeight: FontWeight.bold),
            ),
            Text(
              value.toInt().toString(),
            ),
          ],
        ),
        Slider(
          value: value,
          min: min,
          max: max,
          onChanged: onChanged,
        ),
        const SizedBox(height: 8),
      ],
    );
  }
}

class SocialMediaButton extends StatelessWidget {
  late final Uri uri;
  final String iconPath;

  SocialMediaButton({
    super.key,
    required String url,
    required String icon,
  }) : iconPath = "assets/$icon.svg" {
    uri = Uri.parse(url);
  }

  void _launchURL() async {
    if (await canLaunchUrl(uri)) {
      await launchUrl(uri);
    } else {
      throw 'Could not launch $uri';
    }
  }

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: _launchURL,
      icon: SvgPicture.asset(
        iconPath,
        width: 16,
        height: 16,
        colorFilter: const ColorFilter.mode(Colors.white, BlendMode.srcIn),
      ),
    );
  }
}
