# Fluid Simulation Widget for Flutter

[![Pub Version](https://img.shields.io/pub/v/fluid_simulation)](https://pub.dev/packages/fluid_simulation) [![X (formerly Twitter) Follow](https://img.shields.io/twitter/follow/SixBangs)](https://twitter.com/SixBangs) 


A widget that renders a fluid simulation in Flutter, based on double-density relaxation. Supports interactivity via tap or mouse hover and device orientation.

See it in action: [![try the demo](https://img.shields.io/badge/try_the-demo-8BD0EF)](https://fluid.dumb.fun)

## Features

- Customizable fluid color, particle count, and various simulation parameters
- Tap and drag, hover your mouse, or rotate your device to interact with the fluid
- Two render modes: individual points or blobs (metaballs)

## Installation

Add the package to your `pubspec.yaml` file:

```yaml
dependencies:
  fluid_simulation: ^1.0.4
```

Then add the included shaders to the `flutter` section:

```yaml
flutter:
  # ...
  shaders:
    - packages/fluid_simulation/shaders/gradient.frag
    - packages/fluid_simulation/shaders/blobs.frag
```

Run `flutter pub get` to install the package.

## Usage

Import the widget:

```dart
import 'package:fluid_simulation/fluid_simulation.dart';
```

And add it to your app:

```dart
FluidSimulationWidget(
  color: Colors.blue,
  particleCount: 250,
  gravityMagnitude: 0,
  renderMode: RenderMode.blobs,
)
```

## Customization

The simulation and fluid appearance can be customized with various parameters:

- `color`: The color of the fluid (defaults to the primary color of the current theme)
- `particleCount`: The number of particles in the simulation
- `stiffness`: The stiffness constant for the pressure constraint
- `stiffnessNear`: The stiffness constant for the near-pressure constraint
- `restDensity`: The rest density of the fluid
- `interactionRadius`: The radius around a particle in which it interacts with neighboring particles
- `gravityMagnitude`: The magnitude of the gravity force applied to the particles
- `renderMode`: The render mode of the particles, either as individual points or blobs
- `gradientRadius`: The radius of the density gradient used for rendering blobs
- `pointerEnabled`: Whether the fluid particles are affected by tap or mouse hover
- `orientationEnabled`: Whether gravity is determined by the device orientation. Only supported on mobile.

A child widget is also supported:

- `child`: An optional child widget to render UI elements on top of the fluid simulation

## Related Works

This fluid simulation is based on double-density relaxation and was ported from [LÖVE Fluid Simulation](https://gitlab.com/sixbangs/fluid) with significant help from [Claude](https://claude.ai).

See also:

- [Particle-based Viscoelastic Fluid Simulation](https://gitlab.com/dumbtech/fluid2/-/raw/main/doc/pvfs.pdf): The paper describing double-density relaxation.
- [Simulating Blobs of Fluid](https://peeke.nl/simulating-blobs-of-fluid): The article that inspired the original author to build their first fluid simulation.