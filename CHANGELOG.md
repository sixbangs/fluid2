# 1.0.4

* Add a speed limit to stop particles from going nuts
* Default gravityMagnitude = 0

# 1.0.3

* Fixed the performance issue in the spatial hash package and upgraded to the new version
* Fixed spatial hash usage, making the simulation more stable.
* Removed the antistick hack
* Still a little slow in web, so reduced default and max particles
* Adjusted various settings to compensate

# 1.0.2

* Remove double-density relaxation paper from the package

# 1.0.1

* Small documentation updates
* Fixed the iOS PWA name and icon

# 1.0.0

* Initial release.